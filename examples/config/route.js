export default [
  {
    name: '基础UI组件',
    key: 'base',
    icon: 'mail',

    subRoute: [
      {
        name: 'TableSearch',
        label: '表格条件查询',
        componentId: 'DemoTableSearch',
      },
      {
        name: 'LabelSearch',
        label: '标签条件查询',
        componentId: 'DemoLabelSearch',
      },
      {
        name: 'LatLngSelect',
        label: '经纬度选择',
        componentId: 'DemoLatLngSelect',
      },
      {
        name: 'RichText',
        label: '富文本',
        componentId: 'DemoRichText',
      },
      {
        name: 'FileUpload',
        label: '文件上传',
        componentId: 'DemoFileUpload',
      },
      {
        name: 'ImageUpload',
        label: '图片上传',
        componentId: 'DemoImageUpload',
      },

      {
        name: 'PageWrapper',
        label: '页面包裹层',
        componentId: 'DemoPageWrapper',
      },
      {
        name: 'FormWrapper',
        label: '表单包裹层',
        componentId: 'DemoFormWrapper',
      },
      {
        name: 'TableWrapper',
        label: '表格包裹层',
        componentId: 'DemoTableWrapper',
      },
      {
        name: 'PieChart',
        label: '饼图',
        componentId: 'DemoPieChart',
      },
      {
        name: 'ColumnarChart',
        label: '柱状图',
        componentId: 'DemoColumnarChart',
      },
      {
        name: 'RandarChart',
        label: '雷达图',
        componentId: 'DemoRandarChart',
      },
      {
        name: 'LineChart',
        label: '折现图',
        componentId: 'DemoLineChart',
      },
    ],
  },

  {
    name: '通用页面',
    key: 'page',
    icon: 'mail',

    subRoute: [
      {
        name: 'AuthPage',
        label: '权限管理',
        componentId: 'DemoAuthPage',
      },
      {
        name: 'RolePage',
        label: '角色管理',
        componentId: 'DemoRolePage',
      },
      {
        name: 'MenuPage',
        label: '菜单管理',
        componentId: 'DemoMenuPage',
      },
    ],
  },
];
