import api from '@/api';

import DemoPageWrapper from '../components/DemoPageWrapper.vue';
import DemoFormWrapper from '../components/DemoFormWrapper.vue';
import DemoTableWrapper from '../components/DemoTableWrapper.vue';
import DemoTableSearch from '../components/DemoTableSearch.vue';
import DemoImageUpload from '../components/DemoImageUpload.vue';
import DemoFileUpload from '../components/DemoFileUpload.vue';
import DemoLineChart from '../components/DemoLineChart.vue';
import DemoPieChart from '../components/DemoPieChart.vue';
import DemoRandarChart from '../components/DemoRandarChart.vue';
import DemoColumnarChart from '../components/DemoColumnarChart.vue';
import DemoLabelSearch from '../components/DemoLabelSearch.vue';
import DemoLatLngSelect from '../components/DemoLatLngSelect.vue';
import DemoRichText from '../components/DemoRichText.vue';

import DemoMenuPage from '../pages/DemoMenuPage.vue';
import DemoRolePage from '../pages/DemoRolePage.vue';
import DemoAuthPage from '../pages/DemeAuthPage.vue';

const components = [
  DemoPageWrapper,
  DemoFormWrapper,
  DemoTableWrapper,
  DemoTableSearch,
  DemoImageUpload,
  DemoFileUpload,
  DemoLineChart,
  DemoPieChart,
  DemoRandarChart,
  DemoColumnarChart,
  DemoLabelSearch,
  DemoLatLngSelect,
  DemoRichText,

  DemoMenuPage,
  DemoRolePage,
  DemoAuthPage,
];

export default {
  install(Vue) {
    Vue.prototype.$api = api;

    components.forEach((com) => {
      Vue.component(com.name, com);
    });
  },
};
