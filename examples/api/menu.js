import http from './client';

export default {
  /**
   * 创建菜单
   *
   * @param {*} data
   * @returns
   */
  add(data) {
    return http.post('/kprSystemApi/menu/add', data);
  },
  /**
   * 修改菜单
   *
   * @param {*} data
   * @returns
   */
  update(data) {
    return http.post('/kprSystemApi/menu/update', data);
  },
  /**
   * 删除菜单
   *
   * @param {*} data
   * @returns
   */
  delete(data) {
    return http.post('/kprSystemApi/menu/delete', data);
  },
  /**
   * 获取菜单信息
   *
   * @param {*} data
   * @returns
   */
  info(params) {
    return http.get('/kprSystemApi/menu/getInfo', { params });
  },
  /**
   * 获取所有菜单
   *
   * @param {*} data
   * @returns
   */
  list(params) {
    return http.get('/kprSystemApi/menu/getList', { params });
  },
  /**
   * 分页获取菜单
   *
   * @param {*} data
   * @returns
   */
  page(params) {
    return http.get('/kprSystemApi/menu/getPage', { params });
  },
  /**
   * 获取菜单树形结构
   *
   * @param {*} data
   * @returns
   */
  tree(params) {
    return http.get('/kprSystemApi/menu/getListTree', { params });
  },

  /** ************** 菜单 功能管理 ************** */

  /**
   * 创建功能
   *
   * @param {*} data
   * @returns
   */
  addFunction(data) {
    return http.post('/kprSystemApi/function/add', data);
  },
  /**
   * 修改功能
   *
   * @param {*} data
   * @returns
   */
  updateFunction(data) {
    return http.post('/kprSystemApi/function/update', data);
  },
  /**
   * 删除功能
   *
   * @param {*} data
   * @returns
   */
  deleteFunction(data) {
    return http.post('/kprSystemApi/function/delete', data);
  },
  /**
   * 获取功能信息
   *
   * @param {*} data
   * @returns
   */
  infoFunction(params) {
    return http.get('/kprSystemApi/function/getInfo', { params });
  },
  /**
   * 获取所有功能
   *
   * @param {*} data
   * @returns
   */
  listFunction(params) {
    return http.get('/kprSystemApi/function/getList', { params });
  },
  /**
   * 分页获取功能
   *
   * @param {*} data
   * @returns
   */
  pageFunction(params) {
    return http.get('/kprSystemApi/function/getPage', { params });
  },
};
