import http from './client';

export default {
  /**
   * 创建角色
   *
   * @param {*} data
   * @returns
   */
  add(data) {
    return http.post('/kprSystemApi/role/add', data);
  },
  /**
   * 修改角色
   *
   * @param {*} data
   * @returns
   */
  update(data) {
    return http.post('/kprSystemApi/role/update', data);
  },
  /**
   * 删除角色
   *
   * @param {*} data
   * @returns
   */
  delete(data) {
    return http.post('/kprSystemApi/role/delete', data);
  },
  /**
   * 获取角色信息
   *
   * @param {*} data
   * @returns
   */
  info(params) {
    return http.get('/kprSystemApi/role/getInfo', { params });
  },
  /**
   * 获取所有角色
   *
   * @param {*} data
   * @returns
   */
  list(params) {
    return http.get('/kprSystemApi/role/getList', { params });
  },
  /**
   * 分页获取角色
   *
   * @param {*} data
   * @returns
   */
  page(params) {
    return http.get('/kprSystemApi/role/getPage', { params });
  },
};
