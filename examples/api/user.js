import http from './client';

export default {
  /**
   * 后台用户登录
   *
   * @param {*} data
   * @returns
   */
  login(params) {
    return http.get('/propertyManageApi/public/login', { params });
  },
};
