import md5 from 'md5';
import axios from 'axios';
import con from '../config';

/* eslint-disable */
import { notification } from '../../packages';

const requestTimeCache = {};
const errTxt = '请求失败，网络状况不佳';

const http = axios.create({
  baseURL: '/',
  timeout: 6000,
});

/**
 * 测试方法
 * 实际项目  请求接口获得sign参数
 */
const createTestSign = () => {
  return md5(`${con.appKey}${con.appSecret}`);
}

/**
 * 错误处理
 */
const errorHandler = (error) => {
  if (axios.isCancel(error)) {
    // 路由切换时 取消未完成的请求
    console.log(error.message);
  } else if (typeof error === 'string') {
    notification.error({
      message: '错误',
      description: error,
    });
  } else if (error.response) {
    const { data } = error.response;

    if ([401, 403, 404].includes(error.response.status)) {
      notification.error({
        message: '访问拒绝',
        description: data.message,
      });
    }

    if ([400, 500].includes(error.response.status)) {
      notification.error({
        message: '服务器错误',
        description: '请联系系统管理员',
      });
    }
  }

  return Promise.reject(error);
};

/**
 * request 拦截器
 */
http.interceptors.request.use((config) => {
  const headers = {
    Authorization: 'admin',
    busiId: '0', // 根据实际项目填写，可能为：公司ID, 小区ID
    userId: '1', // 根据实际项目填写, 登录用户ID
    userName: 'Admin', // 根据实际项目填写, 登录用户名称
    appKey: con.appKey,
    sign: createTestSign(),
  };

  Object.assign(config.headers, headers);

  config.params = config.params || {};

  Object.assign(config.params, {
    t: Date.now(),
    platform: 'pc', // 平台标识
  });

  return config;
}, errorHandler);

/**
 * response 拦截器
 */
http.interceptors.response.use((response) => {
  if (response.status === 200) {
    // 文件下载不判断数据状态码
    if (response.config.responseType === 'blob') {
      return Promise.resolve(response.data);
    }

    // 普通数据返回，验证数据状态，错误提示
    if (response.data.status !== '0') {
      errorHandler(response.data.msg);
      return Promise.reject(new Error(response.data.msg));
    }

    return Promise.resolve(response.data);
  }

  errorHandler(errTxt);
  return Promise.reject(new Error(errTxt));
}, errorHandler);

export default http;
