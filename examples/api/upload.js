import http from './client';

export default {
  /** 文件上传 */
  file(data) {
    return http.post('/frame/upload', data.formData, {
      timeout: 0,
      headers: {
        'Content-Type': 'multipart/form-data;charset=UTF-8',
      },
      onUploadProgress: (e) => {
        if (data.progress && typeof data.progress === 'function') {
          data.progress(e.loaded / e.total);
        }
      },
    });
  },
  /** 文件上传 */
  fileExcel(data) {
    return http.post('/labelCate/importInfo', data.formData, {
      timeout: 0,
      headers: {
        'Content-Type': 'multipart/form-data;charset=UTF-8',
      },
      onUploadProgress: (e) => {
        if (data.progress && typeof data.progress === 'function') {
          data.progress(e.loaded / e.total);
        }
      },
    });
  },
};
