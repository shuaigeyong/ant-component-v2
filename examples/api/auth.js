import http from './client';

export default {
  /**
   * 创建权限
   *
   * @param {*} data
   * @returns
   */
  add(data) {
    return http.post('/kprSystemApi/authority/add', data);
  },
  /**
   * 修改权限
   *
   * @param {*} data
   * @returns
   */
  update(data) {
    return http.post('/kprSystemApi/authority/update', data);
  },
  /**
   * 删除权限
   *
   * @param {*} data
   * @returns
   */
  delete(data) {
    return http.post('/kprSystemApi/authority/delete', data);
  },
  /**
   * 获取权限信息
   *
   * @param {*} data
   * @returns
   */
  info(params) {
    return http.get('/kprSystemApi/authority/getInfo', { params });
  },
  /**
   * 获取所有权限
   *
   * @param {*} data
   * @returns
   */
  list(params) {
    return http.get('/kprSystemApi/authority/getList', { params });
  },
  /**
   * 分页获取权限
   *
   * @param {*} data
   * @returns
   */
  page(params) {
    return http.get('/kprSystemApi/authority/getPage', { params });
  },
};
