module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: 'http://183.230.166.148:8090',
        ws: true,
        secure: false,
        changeOrigin: true,
        pathRewrite: {
          '^/api': '',
        },
      },
    },
  },
};
