import Vue from 'vue';
import antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';

import App from './App.vue';

import packages from '../packages';
import plugin from './plugins';

Vue.config.productionTip = false;

Vue.use(antd);
Vue.use(packages);
Vue.use(plugin);

new Vue({
  render: (h) => h(App),
}).$mount('#app');
