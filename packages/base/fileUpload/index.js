import FileUpload from './src/Index.vue';

FileUpload.install = (Vue) => {
  Vue.component(FileUpload.name, FileUpload);
};

export default FileUpload;
