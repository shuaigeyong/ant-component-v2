import TableWrapper from './src/Index.vue';

TableWrapper.install = (Vue) => {
  Vue.component(TableWrapper.name, TableWrapper);
};

export default TableWrapper;
