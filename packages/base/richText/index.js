import RichText from './src/Index.vue';

RichText.install = (Vue) => {
  Vue.component(RichText.name, RichText);
};

export default RichText;
