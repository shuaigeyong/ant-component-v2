import FormWrapper from './src/Index.vue';

FormWrapper.install = (Vue) => {
  Vue.component(FormWrapper.name, FormWrapper);
};

export default FormWrapper;
