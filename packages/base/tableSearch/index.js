import config from '../../config';
import InputItem from './src/components/InputItem.vue';
import SelectItem from './src/components/SelectItem.vue';
import DateItem from './src/components/DateItem.vue';
import DateRangeItem from './src/components/DateRangeItem.vue';
import NumberRangeItem from './src/components/NumberRange.vue';
import DateYearItem from './src/components/DateYearItem.vue';
import DateMonthItem from './src/components/DateMonthItem.vue';
import CascaderItem from './src/components/CascaderItem.vue';

const TableSearch = {
  name: 'TableSearch',
  components: {
    InputItem,
    SelectItem,
    DateItem,
    DateRangeItem,
    NumberRangeItem,
    DateYearItem,
    DateMonthItem,
    CascaderItem,
  },
  props: {
    cache: {
      type: Boolean,
      default: false,
    },
    allowClear: {
      type: Boolean,
      default: true,
    },
    isMore: {
      type: Boolean,
      default: false,
    },
    value: {
      type: Object,
      default() {
        return {};
      },
    },
    /**
     * 查询条件展示几列
     */
    cols: {
      type: Number,
      default: 4,
    },
    /**
     * 查询字段列表
     */
    columns: {
      type: Array,
      default() {
        return [];
      },
    },
    /**
     * 外层采用a-card包裹
     */
    isCard: {
      type: Boolean,
      default: true,
    },
    /**
     * 自由模式
     */
    free: {
      type: Boolean,
      default: false,
    },
    freeWidth: {
      type: Number,
      default: 250,
    },
  },
  data() {
    return {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
      formData: {},
      showMore: false,
    };
  },
  computed: {
    colspan() {
      return 24 / this.cols;
    },
    columnsArr() {
      // 将显示的数组转化为一个二维数组
      const num = this.cols;
      const colArr = new Array(Math.ceil(this.columns.length / num));
      for (let i = 0; i < colArr.length; i += 1) {
        colArr[i] = [];
        for (let j = 0; j < num; j += 1) {
          colArr[i][j] = '';
        }
      }

      for (let i = 0; i < this.columns.length; i += 1) {
        const item = this.columns[i];
        this.$set(this.formData, item.field, this.value[item.field] || undefined);
        colArr[parseInt(i / num, 10)][i % num] = item;
      }

      return colArr;
    },
  },
  mounted() {
    this.showMore = this.isMore;
    this.$nextTick(() => {
      this.getCache();
    });
  },
  methods: {
    confirmKeyUpHandler(e) {
      e.preventDefault();
      if (e.keyCode === 13) {
        this.emitData();
      }
    },
    emitData() {
      this.$emit('input', this.formData);
      this.$emit('on-search', this.formData);
      this.cacheHandler();
    },

    cacheHandler() {
      if (this.cache) {
        const { path } = this.$route;
        window.tableSearchCache = window.tableSearchCache || {};
        window.tableSearchCache[path] = this.formData;
      }
    },
    getCache() {
      if (this.cache) {
        const { path } = this.$route;
        window.tableSearchCache = window.tableSearchCache || {};
        const data = window.tableSearchCache[path] || {};
        Object.assign(this.formData, data);
        this.emitData();
      }
    },
    resetHandler() {
      Object.keys(this.formData).forEach((key) => {
        const type = Object.prototype.toString.call(this.formData[key]);

        switch (type) {
          case '[object Number]':
          case '[object String]':
            this.formData[key] = '';
            break;
          case '[object Array]':
            this.formData[key] = [];
            break;
          default:
            break;
        }
      });
      this.emitData();
    },
    moreFieldsHandler() {
      this.showMore = !this.showMore;
    },
    getComponentName(type = 'input') {
      const key = type.replace(/\b(\w)(\w*)/g, ($0, $1, $2) => $1.toUpperCase() + $2);
      return `${key}Item`;
    },
    componentValueChange(col, value, callback) {
      this.formData[col.field] = value;
      if (callback) {
        // 触发更新
        this.$emit('input', this.formData);
      }
    },
    buildRows(h) {
      const rows = [];

      const freeItems = [];

      this.columnsArr.forEach((row, rowIndex) => {
        const cols = [];

        if (!this.showMore && rowIndex > 0) {
          return;
        }

        row.forEach((col, colIndex) => {
          if (!col) {
            return;
          }

          const el = h(this.getComponentName(col.type), {
            props: {
              allowClear: this.allowClear,
              ...col,
              value: this.formData[col.field],
            },
            style: this.free ? {
              width: `${col.freeWidth || this.freeWidth}px`,
              marginRight: '10px',
              marginBottom: '5px',
            } : {},
            on: {
              input: this.componentValueChange.bind(this, col),
            },
          });

          const colEl = h('a-col', {
            props: {
              key: `search_row_${rowIndex}_col_${colIndex}`,
              span: this.colspan,
            },
          }, [el]);

          cols.push(colEl);
          freeItems.push(el);
        });

        const rowEl = h('a-row', {
          props: {
            key: `search_row_${rowIndex}`,
            gutter: 8,
          },
        }, cols);
        rows.push(rowEl);
      });

      if (this.free) {
        return [h('div', {
          class: 'free-container',
        }, freeItems)];
      }

      return rows;
    },
    buildBtns(h) {
      const resetBtn = h('a-button', {
        props: { type: 'dashed', icon: 'rollback' },
        on: {
          click: this.resetHandler,
        },
      }, '重置');

      const searchBtn = h('a-button', {
        props: { type: 'primary', icon: 'search' },
        on: {
          click: this.emitData,
        },
      }, '搜索');

      const moreBtn = h('a-button', {
        props: { type: 'link', icon: this.showMore ? 'up' : 'down', size: 'small' },
        on: {
          click: this.moreFieldsHandler,
        },
      }, this.showMore ? '收起' : '更多条件');

      const child = [
        resetBtn,
        searchBtn,
      ];

      if (this.columnsArr.length > 1 && !this.free) {
        child.unshift(moreBtn);
      }

      return h('div', {
        class: 'btns',
      }, child);
    },
  },
  render(h) {
    const rows = this.buildRows(h);
    const btns = this.buildBtns(h);

    const form = h('a-form-model', {
      props: {
        labelAlign: this.free ? 'left' : 'right',
        labelCol: this.labelCol,
        wrapperCol: this.wrapperCol,
        model: this.formData,
      },
    }, [
      h('a-input', { style: { display: 'none' }, props: { placeholder: '这个框是为了避免只有一个Input时点击回车页面被刷新留下的' } }),
      ...rows,
    ]);

    return h(this.isCard ? 'a-card' : 'div', {
      props: {
        bordered: false,
      },
      class: `${config.prefixCls}-table-search`,
      on: {
        keyup: this.confirmKeyUpHandler,
      },
    }, [form, btns]);
  },
};

TableSearch.install = (Vue) => {
  Vue.component(TableSearch.name, TableSearch);
};

export default TableSearch;
