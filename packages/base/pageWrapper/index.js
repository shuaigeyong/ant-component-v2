import PageWrapper from './src/Index.vue';

PageWrapper.install = (Vue) => {
  Vue.component(PageWrapper.name, PageWrapper);
};

export default PageWrapper;
