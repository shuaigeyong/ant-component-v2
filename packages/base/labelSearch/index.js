import LabelSearch from './src/Index.vue';

LabelSearch.install = (Vue) => {
  Vue.component(LabelSearch.name, LabelSearch);
};

export default LabelSearch;
