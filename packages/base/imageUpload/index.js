import ImageUpload from './src/Index.vue';

ImageUpload.install = (Vue) => {
  Vue.component(ImageUpload.name, ImageUpload);
};

export default ImageUpload;
