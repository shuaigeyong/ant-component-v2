import LatLngSelect from './src/Index.vue';

LatLngSelect.install = (Vue) => {
  Vue.component(LatLngSelect.name, LatLngSelect);
};

export default LatLngSelect;
