import LineChart from './src/line/LineCharts.vue';
import ColumnarChart from './src/columnar/ColumnarCharts.vue';
import PieChart from './src/pie/PieChart.vue';
import RandarChart from './src/randar/RandarChart.vue';

const plugin = {};
plugin.install = (Vue) => {
  Vue.component(LineChart.name, LineChart);
  Vue.component(ColumnarChart.name, ColumnarChart);
  Vue.component(PieChart.name, PieChart);
  Vue.component(RandarChart.name, RandarChart);
};

export default plugin;
