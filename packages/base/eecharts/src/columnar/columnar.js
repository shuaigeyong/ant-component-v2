import common from '../hook/common';
import configure from '../hook/configure';

let colorArr = [];

// #region 默认配置

// series
const defaultCell = {
  type: 'pictorialBar',
  itemStyle: {
    normal: {
      color: '#fff',
    },
  },
  symbolRepeat: 'fixed',
  symbolMargin: 6,
  symbol: 'rect',
  symbolClip: true,
  symbolSize: ['98%', 2],
  symbolPosition: 'start',
  symbolOffset: [0, -1],
  data: [],
  z: 0,
  zlevel: 1,
};

// series
const defaultSeries = {
  name: '',
  type: 'bar',
  label: {
    normal: {
      show: false,
      lineHeight: common.proportion(30, false),
      formatter: '{c}',
      position: 'top',
      textStyle: {
        color: '#00D6F9',
        fontSize: common.proportion(15, false),
      },
    },
  },
  itemStyle: {
    normal: {
      color: '',
      shadowColor: 'rgb(0,160,221,0)',
      shadowBlur: common.proportion(0, false),
    },
  },
  data: [],
};

// xAxis
const defaultXaxis = {
  boundaryGap: true,
};

// tooltip
const defaultTooltip = {
  trigger: 'axis',
  axisPointer: {
    type: 'shadow',
  },
};

// #endregion

// 数据项配置
function seriesConfig(config = {}, dataSet = [], custom = {}, convenient = {}) {
  if (common.series) {
    if (!['[object Object]', '[object Array]'].includes(common.typeJudge(config.series, true))) {
      console.error('Error info: "config.series", You should pass a two-dimensional array or object！\n Error source: columnar-chart');
      return {};
    }

    if (Array.isArray(config.series) && !common.arrayDimension(config.series)) {
      console.error('Error info: "config.series", If you want to use array type, you should pass a two-dimensional array！\n Error source: columnar-chart');
      return {};
    }
  }

  colorArr = [];
  const series = [];
  const paramsSeries = Array.isArray(config.series) ? config.series : JSON.parse(JSON.stringify(config.series || {}));
  dataSet.forEach((item, index) => {
    const datas = common.dataPulloff(item);
    const configs = JSON.parse(JSON.stringify(defaultSeries));
    configs.name = datas.title;
    configs.data = datas.value;
    configs.itemStyle.normal.color = convenient.areaColor[index] ?? (custom.gradient ? common.createColor(true, '1') : common.createColor(false, '1'));

    const mergeConfig = common.typeJudge(paramsSeries, 'Object') ? paramsSeries : paramsSeries?.[index]?.[0] ?? {};

    series.push(common.deepObjectMerge(configs, mergeConfig));
    colorArr.push(series[index].itemStyle.normal.color);
  });

  return {
    series,
    color: colorArr,
  };
}

/**
 * 图形功能支持配置
 */
function chartType(config = {}, dataSet = [], custom = {}) {
  const dataAll = common.dataPulloff(dataSet[0]);

  if (custom.scroll) {
    let dataZoom = JSON.parse(JSON.stringify(configure.dataZoom));
    if (['xCell', 'x'].includes(custom.type)) {
      dataZoom = common.removeKeys(dataZoom[0], ['xAxisIndex']);
      dataZoom.yAxisIndex = [0];
      dataZoom = [dataZoom];

      config.xAxis = [common.removeKeys(config.xAxis[0], ['boundaryGap'])];
    }

    config.dataZoom = dataZoom;
  }

  // 纵向
  if (['x', 'xCell'].includes(custom.type)) {
    config.yAxis[0].type = 'category';
    config.yAxis[0].data = dataAll.name;
    config.xAxis[0].type = 'value';
  }

  if (['cell', 'xCell'].includes(custom.type)) {
    const all = [];
    config.series.forEach((item) => {
      all.push(item);

      const json = JSON.parse(JSON.stringify(defaultCell));
      json.data = item.data;

      if (custom.type === 'xCell') {
        json.z = 2;
        json.symbol = 'roundRect';
        json.symbolSize = [2, '98%'];
        // json.animationEasing = 'elasticOut';
      }
      all.push(json);
    });

    config.series = all;
    config.tooltip.formatter = (r) => `${r[0].name}</br>${r[0].marker} ${r[0].seriesName} ${r[0].value}`;
  }

  return config;
}

/**
 * 数据整合
 */
function dataIntegration(config = {}, dataSet = [], custom = {}, convenient = {}) {
  if ((!common.arrayDimension(dataSet) || !common.typeJudge(dataSet, 'Array')) && !['xCell', 'cell'].includes(custom.type)) {
    console.error('Error info: "datas", You should pass a two-dimensional array！\n Error source: columnar-chart');
    return {};
  }

  if (!common.typeJudge(config, 'Object')) {
    console.error('Error info: "config", You should pass an "Object"！ \n Error source：columnar-chart');
    return {};
  }

  const legend = JSON.parse(JSON.stringify(configure.legend));
  legend.data = dataSet.map((r) => r[0].title);

  let xAxis = JSON.parse(JSON.stringify(configure.xAxis));
  xAxis = common.deepObjectMerge(xAxis, defaultXaxis);
  xAxis.data = dataSet[0].map((r) => r.name);
  xAxis = common.axisMerge(xAxis, config.xAxis);

  let tooltip = JSON.parse(JSON.stringify(configure.tooltip));
  tooltip = common.deepObjectMerge(tooltip, defaultTooltip);

  function copyObject(target) {
    return JSON.parse(JSON.stringify(target));
  }

  let option = {
    grid: config.grid ? Object.assign(copyObject(configure.grid), config.grid) : copyObject(configure.grid),
    tooltip: config.tooltip ? common.deepObjectMerge(tooltip, config.tooltip) : tooltip,
    legend: config.legend ? common.deepObjectMerge(legend, config.legend) : legend,
    xAxis,
    yAxis: common.axisMerge(copyObject(configure.yAxis), config.yAxis),
    ...seriesConfig(config, dataSet, custom, convenient),
  };

  option = chartType(option, dataSet, custom);

  Object.keys(config).forEach((key) => {
    if (!common.hasOwnPropertys(option, key) && !['convenient', 'width', 'height'].includes(key)) {
      option[key] = config[key];
    }
  });

  return option;
}

export default {
  seriesConfig,
  dataIntegration,
};
