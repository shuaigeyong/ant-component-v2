import common from '../hook/common';
import configure from '../hook/configure';

// series 默认配置
const defaultSeries = {
  type: 'radar',
  data: [],
  symbol: 'circle',
  lineStyle: {
    type: 'solid',
  },
  emphasis: {
    lineStyle: {
      width: common.proportion(4, false),
    },
  },
  shape: 'polygon',
};

/**
 * 数据项配置
 * @param {*} config
 * @param {*} dataSet
 * @param {*} convenient
 */
function seriesConfig(config = {}, dataSet = [], convenient = {}) {
  if (config.series) {
    console.error(`
      Error info: Sorry, in the current version, you cannot change the “series” property, but you can modify the graphic style through another parameter ”seriesData“. You will find it in the user documentation for detailed instructions.
      \n Error source: randar-chart`);
    return {};
  }

  if (config.seriesData) {
    if (!['[object Object]', '[object Array]'].includes(common.typeJudge(config.seriesData, true))) {
      console.error('Error info: "config.series", You should pass a two-dimensional array or object！\n Error source: randar-chart');
      return {};
    }

    if (Array.isArray(config.seriesData) && !common.arrayDimension(config.seriesData)) {
      console.error('Error info: "seriesData", You passed an array, but it is not a two-dimensional array \n Error source: randar-chart');
      return {};
    }
  }

  let paramsSeries = {};
  if (config.seriesData) {
    paramsSeries = common.typeJudge(config.seriesData, 'Object') ? JSON.parse(JSON.stringify(config.seriesData)) : config.seriesData;
  }

  const userColor = {
    areaColor: convenient.areaColor ?? [],
    lineColor: convenient.lineColor ?? [],
  };

  const series = [{
    ...JSON.parse(JSON.stringify(defaultSeries)),
  }];

  // 速配
  series[0].lineStyle.type = convenient.lineType || 'solid';
  series[0].symbol = convenient.symbol || 'circle';

  dataSet.forEach((item, index) => {
    const dataAll = common.dataPulloff(item);
    const defaultDataConfig = {
      value: dataAll.value,
      areaStyle: {
        color: userColor.areaColor[index] ?? common.createColor(true),
      },
      lineStyle: {
        color: userColor.lineColor[index] ?? common.createColor(false, 1),
      },
    };

    const userConfig = common.typeJudge(paramsSeries, 'Object') ? paramsSeries : paramsSeries?.[index]?.[0] ?? {};

    series[0].data.push(common.deepObjectMerge(defaultDataConfig, userConfig));
  });

  return {
    series,
  };
}

/**
 * 数据整合
 */

function dataIntegration(config = {}, dataSet = [], convenient = {}) {
  if (!common.arrayDimension(dataSet) || !common.typeJudge(dataSet, 'Array')) {
    console.error('Error info: "datas", You should pass a two-dimensional array！\n Error source: randar-chart');
    return {};
  }

  if (!common.typeJudge(config, 'Object')) {
    console.error('Error info: "config", You should pass an "Object"！ \n Error source：randar-chart');
    return {};
  }

  if (!common.typeJudge(config.radar || {}, 'Object')) {
    console.error('Error info: "config.radar", Please note that the "radar" attribute is defined as an object, which means that you cannot pass data types other than the object. \n Error source：randar-chart');
    return {};
  }

  const legend = JSON.parse(JSON.stringify(configure.legend));
  legend.data = dataSet[0].map((r) => r[0]?.title ?? '');

  const radar = {
    indicator: [],
  };

  // 速配
  if (convenient.center) radar.center = convenient.center;
  if (convenient.radius) radar.radius = convenient.radius;
  if (convenient.shape) radar.shape = convenient.shape;

  // 最大值存贮
  const mapValueAll = new Map();
  const maxValFunction = (source, target) => source.forEach((item) => {
    if (Array.isArray(item)) {
      maxValFunction(item, target);
    } else if (item.name === target) {
      if (mapValueAll.get(item.name)) {
        mapValueAll.set(item.name, [...mapValueAll.get(item.name), item.value]);
      } else {
        mapValueAll.set(item.name, [item.value || 0]);
      }
    }
  });

  mapValueAll.clear();
  dataSet[0].forEach((item) => {
    maxValFunction(dataSet, item.name);
    const currentValue = mapValueAll.get(item.name);
    const max = Math.max.apply(null, currentValue);
    const min = Math.min.apply(null, currentValue);
    radar.indicator.push({
      name: item.name,
      max: max + min,
    });
  });

  function copyObject(target) {
    return JSON.parse(JSON.stringify(target));
  }

  const option = {
    grid: config.grid ? Object.assign(copyObject(configure.grid), config.grid) : copyObject(configure.grid),
    radar: config.radar ? common.deepObjectMerge(radar, config.radar) : radar,
    tooltip: config.tooltip ? common.deepObjectMerge(configure.tooltip, config.tooltip) : configure.tooltip,
    legend: config.legend ? common.deepObjectMerge(legend, config.legend) : legend,
    ...seriesConfig(config, dataSet, convenient),
  };

  Object.keys(config).forEach((key) => {
    if (!common.hasOwnPropertys(option, key) && !['convenient', 'width', 'height'].includes(key)) {
      option[key] = config[key];
    }
  });

  return option;
}

export default {
  seriesConfig,
  dataIntegration,
};
