import common from '../hook/common';
import configure from '../hook/configure';

// #region 默认配置数据

// series
const defaultSeries = {
  name: '',
  type: 'pie',
  center: ['35%', '50%'],
  clockwise: false,
  avoidLabelOverlap: false,
  label: {
    normal: {
      show: true,
      formatter(parms) {
        return parms.data.name;
      },
    },
  },
  labelLine: {
    normal: {
      length: common.proportion(15, false),
      length2: common.proportion(8, false),
      smooth: true,
    },
  },
  data: [],
};

const defaultItemStyle = {
  borderRadius: common.proportion(10, false),
  borderColor: '#fff',
  borderWidth: common.proportion(2, false),
};

const defaultLegend = {
  orient: 'vertical',
  left: '70%',
  top: 'middle',
  itemGap: common.proportion(10, false),
  show: true,
  textStyle: {
    color: '#8C8C8C',
  },
};

const defaultTooltip = {
  trigger: 'item',
  formatter(parms) {
    const str = `${parms.seriesName}</br>${
      parms.marker}${parms.data.name}</br>`
      + `数值：${parms.data.value}</br>`
      + `占比：${parms.percent}%`;
    return str;
  },
};

// #endregion

/**
 * 饼图类型基础配置
 */
function pieChartType(config = {}, custom = {}) {
  switch (custom.type) {
    case 'ring':
      config.radius = ['40%', '65%'];
      break;
    case 'basis':
      config.radius = '70%';
      break;
    case 'gapRing':
      config.radius = ['40%', '65%'];
      config.itemStyle = defaultItemStyle;
      break;
    case 'basisRing':
      config.radius = '70%';
      config.itemStyle = defaultItemStyle;
      break;
    case 'rose':
      config.radius = ['20%', '65%'];
      config.roseType = 'area';
      config.itemStyle = {
        borderRadius: common.proportion(8, false),
      };
      break;
    default:
      break;
  }

  return config;
}

/**
 * 数据项配置
 * @param {*} config
 * @param {*} dataSet
 */
function seriesConfig(config = {}, dataSet = [], custom = {}) {
  const series = [];

  const paramsSeries = JSON.parse(JSON.stringify(config?.series ?? {}));
  let configs = JSON.parse(JSON.stringify(defaultSeries));

  const datas = common.dataPulloff(dataSet);
  configs.name = datas.title;
  configs.data = dataSet;

  configs = pieChartType(configs, custom);

  series.push(common.deepObjectMerge(configs, paramsSeries));
  return {
    series,
  };
}

/**
 * 数据整合
 */
function dataIntegration(config = {}, dataSet = [], custom = {}, convenient = {}) {
  if (common.arrayDimension(dataSet, true) > 0 || !common.typeJudge(dataSet, 'Array')) {
    console.error('error info: "datas", which should be a one-dimensional array with each item of type "Object"！ \n error source: pie-chart');
    return {};
  }

  const dataAll = common.dataPulloff(dataSet);

  let legend = JSON.parse(JSON.stringify(configure.legend));
  legend = common.deepObjectMerge(legend, defaultLegend);
  legend = common.removeKeys(legend, ['data', 'righ']);

  const title = JSON.parse(JSON.stringify(configure.title));
  let total = 0;
  dataAll.value.forEach((item) => {
    total += item;
  });
  title.subtext = total;
  title.show = convenient.isTitle;

  let tooltip = JSON.parse(JSON.stringify(configure.tooltip));
  tooltip = common.deepObjectMerge(tooltip, defaultTooltip);

  const userColor = convenient.areaColor;

  const colorAll = new Array(dataSet.length - userColor.length).fill().map(() => (custom.gradient ? common.createColor(true, '1') : common.createColor(false, '1')));

  function copyObject(target) {
    return JSON.parse(JSON.stringify(target));
  }

  const option = {
    grid: config.grid ? Object.assign(copyObject(configure.grid), config.grid) : copyObject(configure.grid),
    title: config.title ? common.deepObjectMerge(title, config.title) : title,
    tooltip: config.tooltip ? common.deepObjectMerge(tooltip, config.tooltip) : tooltip,
    legend: config.legend ? common.deepObjectMerge(legend, config.legend) : legend,
    color: userColor.concat(colorAll),
    ...seriesConfig(config, dataSet, custom),
  };

  Object.keys(config).forEach((key) => {
    if (!common.hasOwnPropertys(option, key) && !['convenient', 'width', 'height'].includes(key)) {
      option[key] = config[key];
    }
  });

  return option;
}

export default {
  seriesConfig,
  dataIntegration,
};
