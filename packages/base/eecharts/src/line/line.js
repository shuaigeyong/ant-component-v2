import common from '../hook/common';
import configure from '../hook/configure';

let colorArr = [];
// series 默认配置
const defaultSeries = {
  name: '',
  type: 'line',
  symbol: 'circle',
  showAllSymbol: true,
  symbolSize: 0,
  smooth: true,

  lineStyle: {
    normal: {
      width: common.proportion(3, false),
      color: '#fff',
    },
    borderColor: 'rgba(0,0,0,.4)',
  },
  areaStyle: {
    normal: {
      color: common.createColor(true),
    },
  },
  data: [],
};

/**
 * 数据项配置
 */
function seriesConfig(config = {}, dataSet = [], convenient = {}) {
  if (common.series) {
    if (!['[object Object]', '[object Array]'].includes(common.typeJudge(config.series, true))) {
      console.error('Error info: "config.series", You should pass a two-dimensional array or object！\n Error source: line-chart');
      return {};
    }

    if (Array.isArray(config.series) && !common.arrayDimension(config.series)) {
      console.error('Error info: "config.series", If you want to use array type, you should pass a two-dimensional array！\n Error source: line-chart');
      return {};
    }
  }

  colorArr = [];
  const series = [];

  let paramsSeries = {};
  if (config.series) {
    paramsSeries = common.typeJudge(config.series, 'Object') ? JSON.parse(JSON.stringify(config.series)) : config.series;
  }

  // 快速配置
  if (common.hasOwnPropertys(convenient, 'smooth') && !convenient.smooth) {
    defaultSeries.smooth = false;
  }

  const userColor = {
    areaColor: convenient.areaColor ?? [],
    lineColor: convenient.lineColor ?? [],
  };

  dataSet.forEach((item, index) => {
    const datas = common.dataPulloff(item);
    const configs = JSON.parse(JSON.stringify(defaultSeries));
    configs.name = datas.title;
    configs.data = datas.value;
    configs.areaStyle.normal.color = userColor.areaColor[index] ?? common.createColor(true);
    configs.lineStyle.normal.color = userColor.lineColor[index] ?? common.createColor(false, '1');

    const mergeConfig = common.typeJudge(paramsSeries, 'Object') ? paramsSeries : paramsSeries?.[index]?.[0] ?? {};

    series.push(common.deepObjectMerge(configs, mergeConfig));
    colorArr.push(series[index].lineStyle.normal.color);
  });

  return {
    series,
    color: colorArr,
  };
}

/**
 * 数据整合
 */
function dataIntegration(config = {}, dataSet = [], convenient = {}) {
  if (!common.arrayDimension(dataSet) || !common.typeJudge(dataSet, 'Array')) {
    console.error('Error info: "datas", You should pass a two-dimensional array！\n Error source: line-chart');
    return {};
  }

  if (!common.typeJudge(config, 'Object')) {
    console.error('Error info: "config", You should pass an "Object"！ \n Error source：line-chart');
    return {};
  }

  const legend = JSON.parse(JSON.stringify(configure.legend));
  legend.data = dataSet.map((r) => r[0].title);

  let xAxis = JSON.parse(JSON.stringify(configure.xAxis));
  xAxis.data = dataSet[0].map((r) => r.name);
  xAxis = common.axisMerge(xAxis, config.xAxis);

  function copyObject(target) {
    return JSON.parse(JSON.stringify(target));
  }

  const option = {
    grid: config.grid ? Object.assign(copyObject(configure.grid), config.grid) : copyObject(configure.grid),
    tooltip: config.tooltip ? common.deepObjectMerge(configure.tooltip, config.tooltip) : configure.tooltip,
    legend: config.legend ? common.deepObjectMerge(legend, config.legend) : legend,
    xAxis,
    yAxis: common.axisMerge(configure.yAxis, config.yAxis),
    ...seriesConfig(config, dataSet, convenient),
  };

  Object.keys(config).forEach((key) => {
    if (!common.hasOwnPropertys(option, key) && !['convenient', 'width', 'height'].includes(key)) {
      option[key] = config[key];
    }
  });

  return option;
}

export default {
  seriesConfig,
  dataIntegration,
};
