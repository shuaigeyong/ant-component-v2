import * as echarts from 'echarts';
import vue from 'vue';

// 大屏支持，Vue原型链方法$d拷贝
function copyPrototypeD(size) {
  return vue?.prototype?.$d?.(size) ?? size;
}

// 大屏比列转换
function proportion(size = 1, unit = 'px') {
  return unit ? `${copyPrototypeD(size)}${unit}` : Number(copyPrototypeD(size));
}

// 键值判断
function hasOwnPropertys(target, key) {
  return Object.prototype.hasOwnProperty.call(target, key);
}

/**
 * 变量类型判断
 */
function typeJudge(target, type) {
  return type === true
    ? Object.prototype.toString.call(target)
    : Object.prototype.toString.call(target).includes(type);
}

/**
 * 二维数组判断
 */

function arrayDimension(dataSet = [], state = false) {
  return state
    ? dataSet.filter((r) => Array.isArray(r)).length
    : dataSet.filter((r) => Array.isArray(r)).length === dataSet.length;
}

/**
 * 对象关键词过滤
 */
function removeKeys(target = {}, key = []) {
  const res = {};
  Object.keys(target).forEach((keys) => {
    if (!key.includes(keys)) {
      res[keys] = target[keys];
    }
  });

  return res;
}

/**
 * 生成随机色
 */
function createColor(gradual = false, opacity = '.5') {
  function randomColor() {
    const r = Math.floor(Math.random() * 256);
    const g = Math.floor(Math.random() * 256);
    const b = Math.floor(Math.random() * 256);
    return `rgba(${r},${g},${b},${opacity})`;
  }

  function randomGradualChange() {
    return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
      offset: 0,
      color: createColor(false),
    },
    {
      offset: 1,
      color: 'rgba(25,163,223, 0)',
    }], false);
  }

  return gradual ? randomGradualChange() : randomColor();
}

/**
 * 深层对象合并
 */
function deepObjectMerge(target, source) {
  if (!typeJudge(source, 'Object')) return target;
  if (!typeJudge(target, 'Object')) return source;

  Object.keys(target).forEach((key) => {
    if (hasOwnPropertys(source, key) && typeJudge(source[key], 'Object')) {
      target[key] = deepObjectMerge(target[key], source[key]);
    } else if (hasOwnPropertys(source, key)) {
      target[key] = source[key];
    }
  });

  Object.keys(source).forEach((key) => {
    if (!target[key]) {
      target[key] = source[key];
    }
  });

  return target;
}

// 数据抽离
function dataPulloff(dataSet = []) {
  const title = [...new Set(dataSet.map((r) => r.title))][0];
  const value = dataSet.map((r) => r.value);
  const name = dataSet.map((r) => r.name);
  return {
    title,
    value,
    name,
  };
}

// 轴坐标配置数据合并
function axisMerge(target = [], source = undefined) {
  if (!Array.isArray(target)) target = [target];
  if (!source) return target;

  const res = [];
  const type = typeJudge(source, 'Object');

  target.forEach((item, index) => {
    res.push(deepObjectMerge(item, type ? source : source?.[index] ?? {}));
  });

  return res;
}

export default {
  copyPrototypeD,
  proportion,
  hasOwnPropertys,
  typeJudge,
  createColor,
  deepObjectMerge,
  dataPulloff,
  arrayDimension,
  axisMerge,
  removeKeys,
};
