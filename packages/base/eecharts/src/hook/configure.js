import common from './common';

const grid = {
  top: '5%',
  left: '8%',
  right: '8%',
  bottom: '8%',
};

const title = {
  show: true,
  text: '统计',
  subtext: '副标题',
  textStyle: {
    fontSize: common.proportion(20, false),
    color: 'black',
  },
  subtextStyle: {
    fontSize: common.proportion(25, false),
    color: 'black',
  },
  textAlign: 'center',
  x: '34.5%',
  y: '45%',
};

const tooltip = {
  trigger: 'axis',
  textStyle: {
    color: '#666',
    align: 'left',
    fontSize: common.proportion(16),
  },
};

const legend = {
  type: 'plain',
  align: 'left',
  // right: '10%',
  // top: '2%',
  show: false,
  textStyle: {
    color: '#666',
    fontSize: common.proportion(16),
  },

  itemGap: common.proportion(15, false),
  // itemWidth: common.proportion(30, false),
  icon: 'rich',
  data: [],
};

const defaultAxis = {
  axisLine: {
    show: true,
    lineStyle: {
      color: '#333',
    },
  },
  axisLabel: {
    textStyle: {
      show: true,
      color: '#333',
      padding: common.proportion(16, false),
      fontSize: common.proportion(15),
    },
    formatter: (data) => data,
  },
  splitLine: {
    show: false,
    lineStyle: {
      color: '#192a44',
    },
  },
  axisTick: {
    show: false,
  },
  name: '',
  nameTextStyle: {
    color: '#333',
    fontSize: common.proportion(16),
    padding: common.proportion(10),
  },
};

const xAxis = {
  type: 'category',
  ...defaultAxis,
  boundaryGap: false,
  data: [],
};

const yAxis = {
  type: 'value',
  min: 0,
  ...defaultAxis,
};

const dataZoom = [
  {
    type: 'inside',
    start: 0,
    end: 80,
    xAxisIndex: [0],
  },
];

export default {
  grid,
  tooltip,
  legend,
  xAxis,
  yAxis,
  title,
  dataZoom,
};
