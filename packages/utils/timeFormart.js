import dayjs from 'dayjs';
// TODO： 使用dayjs
export default {
  getWeek(type, dates) {
    const startTime = dayjs().startOf('week').add(1, 'day').format('YYYY-MM-DD');
    dayjs(startTime).valueOf();
    const endTime = dayjs().endOf('week').add(1, 'day').format('YYYY-MM-DD');
    dayjs(endTime).valueOf();
    console.log();
    const now = new Date();
    const nowTime = now.getTime();
    const day = now.getDay();
    const longTime = 24 * 60 * 60 * 1000;
    const n = longTime * 7 * (dates || 0);
    let dd;
    if (type === 's') {
      dd = nowTime - (day - 1) * longTime + n;
    }
    if (type === 'e') {
      dd = nowTime + (7 - day) * longTime + n;
    }
    dd = new Date(dd).getTime();
    return dd;
  },
  getMonth(type, months) {
    const d = new Date();
    let year = d.getFullYear();
    let month = d.getMonth() + 1;
    if (Math.abs(months) > 12) {
      months %= 12;
    }
    if (months !== 0) {
      if (month + months > 12) {
        year += 1;
        month = (month + months) % 12;
      } else if (month + months < 1) {
        year -= 1;
        month = 12 + month + months;
      } else {
        month += months;
      }
    }
    month = month < 10 ? `0${month}` : `${month}`;
    const firstday = `${year}-${month}-01`;
    let lastday = '';
    if (month === '01' || month === '03' || month === '05' || month === '07' || month === '08' || month === '10' || month === '12') {
      lastday = `${year}-${month}-${31}`;
    } else if (month === '02') {
      if ((year % 4 === 0 && year % 100 !== 0) || (year % 100 === 0 && year % 400 === 0)) {
        lastday = `${year}-${month}-${29}`;
      } else {
        lastday = `${year}-${month}-${28}`;
      }
    } else {
      lastday = `${year}-${month}-${30}`;
    }
    let day = '';
    if (type === 's') {
      day = firstday;
    } else {
      day = lastday;
    }
    day = new Date(day).getTime();
    return day;
  },
  getYear(type, dates) {
    const dd = new Date();
    const n = dates || 0;
    const year = dd.getFullYear() + Number(n);
    let day = '';
    if (type === 's') {
      day = `${year}-01-01`;
    }
    if (type === 'e') {
      day = `${year}-12-31`;
    }
    day = new Date(day).getTime();
    return day;
  },
  getAroundTime(data) {
    const day = new Date();
    const time = new Date(day.getTime() - 24 * Number(data) * 60 * 60 * 1000).getTime();
    return time;
  },
};
