import './style/index.less';
import config from './config';

import PageWrapper from './base/pageWrapper';
import FormWrapper from './base/formWrapper';
import TableWrapper from './base/tableWrapper';
import TableSearch from './base/tableSearch';
import ImageUpload from './base/imageUpload';
import FileUpload from './base/fileUpload';
import Eecharts from './base/eecharts';
import LabelSearch from './base/labelSearch';
import LatLngSelect from './base/latLngSelect';
import RichText from './base/richText';

import MenuPage from './page/menu';
import RolePage from './page/role';
import AuthPage from './page/auth';

/* 导出常用三方框架或组件内容  */
export { default as zhCN } from 'ant-design-vue/lib/locale-provider/zh_CN';
export { default as Antd, message, notification } from 'ant-design-vue';
export { default as moment } from 'moment';
export { default as quill } from 'quill';
export { default as ImageResize } from 'quill-image-resize-module';
export { default as quillEditor, Quill } from 'vue-quill-editor';

/* 可用组件列表 */
const components = [
  // 通用组件
  PageWrapper,
  FormWrapper,
  TableWrapper,
  TableSearch,
  ImageUpload,
  FileUpload,
  Eecharts,
  LabelSearch,
  LatLngSelect,
  RichText,

  // 通用页面
  MenuPage,
  RolePage,
  AuthPage,
];
/* 插件初始化 */
const install = (Vue) => {
  if (install.installed) return;
  install.installed = true;
  Vue.prototype.$kprConfig = config;
  components.forEach((component) => {
    Vue.use(component);
    // 注册内部子组件
    if (component.child instanceof Array) {
      component.child.map((c) => Vue.use(c));
    }
  });
};

//  全局引用可自动安装
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}

export default {
  install,
};
