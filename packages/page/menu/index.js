import MenuAddPage from './src/Add.vue';
import MenuListPage from './src/List.vue';

const MenuPage = {
  name: 'MenuPage',
  components: {
    MenuAddPage,
    MenuListPage,
  },
  props: {
    crud: {
      type: Object,
      default() {
        return {};
      },
    },
  },
  data() {
    return {
      current: 'MenuListPage',
      menuId: '',
    };
  },
  methods: {
    backToMenuHandler() {
      this.current = 'MenuListPage';
    },
    addMenuHandler() {
      this.current = 'MenuAddPage';
    },
    editMenuHandler({ id }) {
      this.menuId = id;
      this.current = 'MenuAddPage';
    },
  },
  render(h) {
    return h(this.current, {
      props: {
        menuId: this.menuId,
        ...this.$props.crud,
      },
      on: {
        addMenu: this.addMenuHandler,
        editMenu: this.editMenuHandler,
        backToMenu: this.backToMenuHandler,
      },
    });
  },
};

MenuPage.install = (Vue) => {
  Vue.component(MenuPage.name, MenuPage);
};
MenuPage.child = Object.values(MenuPage.components).map((component) => {
  component.install = (Vue) => {
    Vue.component(component.name, component);
  };
  return component;
});

export default MenuPage;

export {
  MenuAddPage,
  MenuListPage,
};
