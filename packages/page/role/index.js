import RoleAddPage from './src/Add.vue';
import RoleListPage from './src/List.vue';

const Roles = {
  name: 'RolePage',
  components: {
    RoleAddPage,
    RoleListPage,
  },
  props: {
    crud: {
      type: Object,
      default() {
        return {};
      },
    },
  },
  data() {
    return {
      current: 'RoleListPage',
      roleId: '',
    };
  },
  methods: {
    backToRoleHandler() {
      this.current = 'RoleListPage';
    },
    addRoleHandler() {
      this.current = 'RoleAddPage';
    },
    editRoleHandler({ id }) {
      this.roleId = id;
      this.current = 'RoleAddPage';
    },
  },
  render(h) {
    return h(this.current, {
      props: {
        roleId: this.roleId,
        ...this.$props.crud,
      },
      on: {
        addRole: this.addRoleHandler,
        editRole: this.editRoleHandler,
        backToRole: this.backToRoleHandler,
      },
    });
  },
};

Roles.install = (Vue) => {
  Vue.component(Roles.name, Roles);
};
Roles.child = Object.values(Roles.components).map((component) => {
  component.install = (Vue) => {
    Vue.component(component.name, component);
  };
  return component;
});

export default Roles;

export {
  RoleAddPage,
  RoleListPage,
};
