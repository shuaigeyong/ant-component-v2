import AuthAddPage from './src/Add.vue';
import AuthListPage from './src/List.vue';
import VisitPage from './src/Visit.vue';

const Auth = {
  name: 'AuthPage',
  components: {
    AuthAddPage,
    AuthListPage,
    VisitPage,
  },
  props: {
    // 访问授权时使用到的菜单树形结构
    menu: {
      type: Array,
      default() {
        return [];
      },
    },
    crud: {
      type: Object,
      default() {
        return {};
      },
    },
  },
  data() {
    return {
      current: 'AuthListPage',
      authId: '',
    };
  },
  methods: {
    backToAuthHandler() {
      this.current = 'AuthListPage';
    },
    addAuthHandler() {
      this.current = 'AuthAddPage';
    },
    editAuthHandler({ id }) {
      this.authId = id;
      this.current = 'AuthAddPage';
    },
    toVisitHandler({ id }) {
      this.authId = id;
      this.current = 'VisitPage';
    },
  },
  render(h) {
    return h(this.current, {
      props: {
        authId: this.authId,
        menu: this.menu,
        ...this.$props.crud,
      },
      on: {
        addAuth: this.addAuthHandler,
        editAuth: this.editAuthHandler,
        backToAuth: this.backToAuthHandler,
        toVisit: this.toVisitHandler,
      },
    });
  },
};

Auth.install = (Vue) => {
  Vue.component(Auth.name, Auth);
};
Auth.child = Object.values(Auth.components).map((component) => {
  component.install = (Vue) => {
    Vue.component(component.name, component);
  };
  return component;
});

export default Auth;

export {
  AuthAddPage,
  AuthListPage,
};
