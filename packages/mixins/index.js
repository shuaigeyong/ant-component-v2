import { formatDate } from '../utils';

export default {
  filters: {
    date(value, reg) {
      return formatDate(value, reg);
    },
  },
};
