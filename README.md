# ant-component-v2
通用业务、UI、布局、页面模块组件库

## 1.1、npm安装
  ```js
    npm install --save ant-component-v2
  ```

## 1.2、项目内使用
  ```js
    import AntComponent, { Antd } from 'ant-component-v2';
    import 'ant-design-vue/dist/antd.css';
    import 'ant-component-v2/lib/lib.css';
    Vue.use(AntComponent);
    Vue.use(Antd);
  ```

## 1.3、组件列表

  | 名称                | 组件名称                |
  | ------------------- | ---------------------- |
  | 页面包裹层           | PageWrapper            |
  | 表单包裹层           | FormWrapper            |
  | 表格包裹层           | TableWrapper           |
  | 表格条件查询         | TableSearch            |
  | 图片压缩上传         | ImageUpload            |
  | 文件上传             | FileUpload             |
  | 标签条件查询         | LabelSearch            |
  | 经纬度选择           | LatLngSelect           | 
  | 富文本               | RichText               | 

## 2.1、组件集成测试
1. 修改package.json中 main 字段
```js
"main": "lib/lib.umd.min.js" => "main": "packages/index.js",
```
2. 项目根目录下执行
```js
npm link
```
3. 在admin项目根目录下执行
```js
npm link @cqkpr/ant-component
```
4. 在admin项目中正常引入ant-component-v2相关功能进行验证，验证通过后可执行下一步Npm包发布。
tips: 引入后eslint可能会报错，只需要临时禁用掉单行eslint /* eslint-disable */

## 2.2 组件打包发布Npm
1. 修改package.json中 main 字段
```js
"main": "main": "packages/index.js" => "lib/lib.umd.min.js"
```
2. 项目根目录下执行
```js
npm run build
```
3. 修改项目版本号
4. 登录到 npm
```js
npm login
```
5. 发布
```js
npm publish --access public
```