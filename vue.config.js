const path = require('path');
const webpack = require('webpack');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

module.exports = {
  // 修改默认的入口
  pages: {
    index: {
      entry: 'examples/main.js',
      template: 'public/index.html',
      filename: 'index.html',
    },
  },

  devServer: {
    disableHostCheck: true,
    proxy: {
      '/kprSystemApi': {
        target: 'http://localhost:8888',
        ws: true,
        changeOrigin: true,
      },
    },
  },

  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          globalVars: {
          },
          javascriptEnabled: true,
        },
      },
    },
  },

  configureWebpack: {
    plugins: [
      new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /zh-cn$/),
      new BundleAnalyzerPlugin(),
      new webpack.ProvidePlugin({
        'window.Quill': 'quill',
      }),
    ],
    module: {},
    externals: {
    },
  },

  chainWebpack: (config) => {
    // vue默认@指向src目录，这里要修正为examples，另外新增一个~指向packages
    config.resolve.alias
      .set('@', path.resolve('examples'))
      .set('~', path.resolve('packages'));

    // lib目录是组件库最终打包好存放的地方，不需要eslint检查
    // examples/docs是存放md文档的地方，也不需要eslint检查
    config.module
      .rule('eslint')
      .exclude.add(path.resolve('lib'))
      .end()
      .exclude.add(path.resolve('examples/docs'))
      .end();

    // packages和examples目录需要加入编译
    config.module
      .rule('js')
      .include.add(/packages/)
      .end()
      .include.add(/examples/)
      .end()
      .use('babel')
      .loader('babel-loader')
      .tap((options) => options);
  },
};
